/**
 * @description An implementation of IHandleMessages which validates the passed in Lead and inserts it.
 */
public with sharing class IncomingLeadHandler implements IHandleMessages {
    @testVisible private static final String INCOMING_LEAD_CHANNEL = 'IncomingLeads';

    public static Set<String> subscribedChannels;
    /**
     * @description Constructs an instance of IncomingLeadHandler.
     */
    public IncomingLeadHandler() {
        subscribedChannels = new Set<String>();
    }

    /**
     * @description Handles a message on a subscribed channel.
     * @param channel The channel emitting the message.
     * @param data The accompanying data for the message.
     * @throws ArgumentNullException if channel is null.
     * @throws ArgumentException if the lead is missing a FirstName.
     */
    public void handleMessage(String channel, Object data) {
        ArgumentNullException.throwIfNull(channel,'channel');

        if(channel == INCOMING_LEAD_CHANNEL){
            Lead l = (Lead)data;
            if(l.FirstName == null){
                throw new ArgumentException('Lead Missing First Name');
            }
            insert l; 
        }
        
    }

    /**
     * @description Gets a list of channels an implementation subscribes to.
     * @return A List<String> of channel names this implementation is subscribed to.
     */
    public static List<String> getSubscribedChannels() {
        List<String> subscribedChannelsList = new List<String>();
        subscribedChannelsList.addAll(subscribedChannels);
        return subscribedChannelsList;
    }

    public static void addChannel(String channel){
        subscribedChannels.add(channel);
    }

    public static void removeChannel(String channel){
        subscribedChannels.remove(channel);
    }
}